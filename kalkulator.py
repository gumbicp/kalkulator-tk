#!/usr/bin/env python3
#-*- coding: utf-8 -*-
#
# plik kalkulator.py
#
# Program prosty kalkulator
#
#
import tkinter as tk
from tkinter import messagebox as messb


BUTTON_NR_WIDTH = 3
BUTTON_DZIALANIA_WIDTH=4

# zestaw cyfr kalkulatora
CYFRY_DEC = ['0','1','2','3','4','5','6','7','8','9']
DZIALANIA = ['*','/','+','-','%']



def handler_validate( var):
    """
    Funkcja sprawdza dane wprowadzane w Entry czy są cyframi.
    :param var:
    :return:
    """
    new_value = var.get()
    try:
        new_value == '' or float(new_value)
        handler_validate.old_value = new_value
    except ValueError:
        var.set(handler_validate.old_value)

handler_validate.old_value = ''


def centrum_ekranu(okno):
    """
    Funkcja centruje nasze okno na ekranie.
    :param okno:
    :return:
    """
    okno.update_idletasks()
    szerokosc = okno.winfo_width()
    ramka_szerokosc = okno.winfo_rootx() - okno.winfo_x()
    okno_szerokosc =  szerokosc + 2 * ramka_szerokosc

    wysokosc = okno.winfo_height()
    pasek_tytul_wys = okno.winfo_rooty() - okno.winfo_y()
    okno_wys = wysokosc + pasek_tytul_wys + ramka_szerokosc

    x = okno.winfo_screenwidth() // 2 - okno_szerokosc // 2
    y = okno.winfo_screenheight() // 2 - okno_wys // 2

    okno.geometry('{}x{}+{}+{}'.format(szerokosc, wysokosc, x, y))
    okno.deiconify()

class Kalkulator(tk.Frame):

    def __init__(self, master=None):
        tk.Frame.__init__(self, master, name='ramka')
        self.grid(column=0, row=0, sticky=tk.N+tk.S+tk.W+tk.E)


        #zmienne do działań artmetycznych
        self.value_x = tk.StringVar()
        self.value_y = tk.StringVar()
        self.value_wynik = tk.StringVar()

        self.value_x.set('0')
        self.value_y.set('0')
        self.value_wynik.set('0')

        # trace wants a callback with nearly useless parameters, fixing with lambda.
        # do walidacji w Entry zmiennej value_x używamy metody trace
        self.value_x.trace('w', lambda nm, idx, mode, var=self.value_x: handler_validate(var))

        # zmienne dla checkbox_... Dex Hex Bin Oct
        self.value_radiobutton = tk.StringVar()
        self.value_radiobutton.set('dec')
        self.create_menubar()
        self.create_widget()

    def create_menubar(self):
        menu = tk.Menu(self)
        filemenu = tk.Menu(menu)
        menu.add_cascade(label="O programie", menu=filemenu)
        filemenu.add_command(label='Info')
        filemenu.add_separator()
        filemenu.add_command(label="Exit", command=self.quit)
        self.master.configure(menu=menu)

    def create_widget(self):
        # resize
        #top = self.winfo_toplevel()
       # top.rowconfigure(0, weight=1)
        #top.columnconfigure(0, weight=1)
        #self.rowconfigure(0, weight=1)
       # self.columnconfigure(0, weight=1)
        #**********************************************
        # Wejście
        self.wejscie = tk.Entry(self, textvariable=self.value_x)
        #self.wejscie.focus()
        # wynik dzialań
        self.wynik_text = tk.Label(self, text="Wynik: ")
        self.wynik = tk.Label(self,textvariable=self.value_wynik)
        # radiobuttony konstruktory
        self.radiobutton_dec = tk.Radiobutton(self, text='Dec',variable=self.value_radiobutton, value='dec')
        self.radiobutton_hex = tk.Radiobutton(self, text='Hex',variable=self.value_radiobutton, value='hex')
        self.radiobutton_bin = tk.Radiobutton(self, text='Bin',variable=self.value_radiobutton, value='bin')
        self.radiobutton_oct = tk.Radiobutton(self, text='Oct',variable=self.value_radiobutton, value='oct')

        #**********************************************
        #dzialania konstruktory
        self.button_zeruj = tk.Button(self, text='C', width=BUTTON_DZIALANIA_WIDTH, command=self.handler_zeruj)
        #self.button_zeruj.bind('<1>', self.handler_zeruj)

        self.button_modulo = tk.Button(self, text='M%', name='modulo', width=BUTTON_DZIALANIA_WIDTH)
        self.button_modulo.bind('<1>', self.handler_dzialania)

        self.button_plus = tk.Button(self, text='+', name='plus', width=BUTTON_DZIALANIA_WIDTH)
        self.button_plus.bind('<1>', self.handler_dzialania)

        self.button_minus = tk.Button(self, text='-', name='minus', width=BUTTON_DZIALANIA_WIDTH)
        self.button_minus.bind('<1>', self.handler_dzialania)

        self.button_mnoz = tk.Button(self, text='*', name='mnozenie', width=BUTTON_DZIALANIA_WIDTH)
        self.button_mnoz.bind('<1>', self.handler_dzialania)

        self.button_dziel = tk.Button(self, text='/', name='dzielenie', width=BUTTON_DZIALANIA_WIDTH)
        self.button_dziel.bind('<1>', self.handler_dzialania)

        self.button_quit = tk.Button(self, text='Quit', command=self.quit, width=BUTTON_DZIALANIA_WIDTH)

        #cyfry konstruktory sluchacze
        self.button_nr1 = tk.Button(self, text='1', width=BUTTON_NR_WIDTH, name='1')
        self.button_nr1.bind("<Button-1>", self.handler_cyfry)

        self.button_nr2 = tk.Button(self, text='2', width=BUTTON_NR_WIDTH, name='2')
        self.button_nr2.bind("<Button-1>", self.handler_cyfry)

        self.button_nr3 = tk.Button(self, text='3', width=BUTTON_NR_WIDTH, name='3')
        self.button_nr3.bind("<Button-1>", self.handler_cyfry)

        self.button_nr4 = tk.Button(self, text='4', width=BUTTON_NR_WIDTH, name='4')
        self.button_nr4.bind("<Button-1>", self.handler_cyfry)

        self.button_nr5 = tk.Button(self, text='5', width=BUTTON_NR_WIDTH, name='5')
        self.button_nr5.bind("<Button-1>", self.handler_cyfry)

        self.button_nr6 = tk.Button(self, text='6', width=BUTTON_NR_WIDTH, name='6')
        self.button_nr6.bind("<Button-1>", self.handler_cyfry)

        self.button_nr7 = tk.Button(self, text='7', width=BUTTON_NR_WIDTH, name='7')
        self.button_nr7.bind("<Button-1>", self.handler_cyfry)

        self.button_nr8 = tk.Button(self, text='8', width=BUTTON_NR_WIDTH, name='8')
        self.button_nr8.bind("<Button-1>", self.handler_cyfry)

        self.button_nr9 = tk.Button(self, text='9', width=BUTTON_NR_WIDTH, name='9')
        self.button_nr9.bind("<Button-1>", self.handler_cyfry)

        self.button_nr0 = tk.Button(self, text='0', width=BUTTON_NR_WIDTH, name='0',)
        self.button_nr0.bind("<Button-1>", self.handler_cyfry)

        self.button_nr_kropka = tk.Button(self, text='.', width=BUTTON_NR_WIDTH, name='kropka')
        self.button_nr_kropka.bind("<Button-1>", self.handler_cyfry)

        self.button_nr_rownosc = tk.Button(self, text='=', width=BUTTON_NR_WIDTH, name='rownasie')
        self.button_nr_rownosc.bind('<1>', self.handler_dzialania)
        #**********************************************
        # --------------- grid ------------------------
        #wynik
        self.wynik.grid(column=4, row=0, columnspan=2, sticky=tk.W)
        self.wynik_text.grid(column=3, row=0, sticky=tk.W)
        # wejscie

        self.wejscie.grid(column=0, row=0, columnspan=3, sticky=tk.W)

        # checkbox rzad 1
        self.radiobutton_dec.grid(column=5, row=1)
        self.radiobutton_hex.grid(column=4, row=1)
        self.radiobutton_bin.grid(column=3, row=1)
        self.radiobutton_oct.grid(column=2, row=1)
        #dzialania kolumny 4,5 rzędy 2,3,4,5
        self.button_zeruj.grid(column=4, row=2, sticky=tk.E)
        self.button_modulo.grid(column=5, row=2, sticky=tk.E)
        self.button_plus.grid(column=4, row=3, sticky=tk.E)
        self.button_minus.grid(column=4, row=4, sticky=tk.E)
        self.button_mnoz.grid(column=5, row=3, sticky=tk.E)
        self.button_dziel.grid(column=5, row=4, sticky=tk.E)
        self.button_quit.grid(column=4,row=5, columnspan=2, sticky=tk.E)

        #cyfry kolumny 0,1,2 rzędy 2,3,4
        self.button_nr1.grid(column=0, row=2, sticky=tk.W)
        self.button_nr2.grid(column=1, row=2, sticky=tk.W)
        self.button_nr3.grid(column=2, row=2, sticky=tk.W)
        self.button_nr4.grid(column=0, row=3, sticky=tk.W)
        self.button_nr5.grid(column=1, row=3, sticky=tk.W)
        self.button_nr6.grid(column=2, row=3, sticky=tk.W)
        self.button_nr7.grid(column=0, row=4, sticky=tk.W)
        self.button_nr8.grid(column=1, row=4, sticky=tk.W)
        self.button_nr9.grid(column=2, row=4, sticky=tk.W)
        self.button_nr_kropka.grid(column=0, row=5, sticky=tk.W)
        self.button_nr0.grid(column=1, row=5, sticky=tk.W)
        self.button_nr_rownosc.grid(column=2, row=5, sticky=tk.W)

    def _is_int(self):
        """Wrapper dla słuchacza
        Funkcja sprawdza czy liczba z Entry jest liczbą całkowitą ( nie ma w niej znaku '.' )
        :return: True jeżeli int
        """
        if '.' in self.value_x.get() or '.' in self.value_y.get():
            return False
        return True

    def _typ_działania(self, znak):
        if self.value_y.get() == '0':
            self.value_wynik.set(znak)
            self.value_y.set(self.value_x.get())
            self.value_x.set('0')
        elif self.value_wynik.get() in DZIALANIA:
            self._dzialaj()

    def _dzialaj(self):
        """Wrapper dla słuchacza
        Funkcja wybiera między działaniami na liczbach całkowitych lub z przecinkiem.
        Ustawia parametry dla funkcji @see def _dzialanie_rowna_sie(self, znak=None, typ=None):
        :return: None
        """
        if not self._is_int():
            self._dzialanie_rowna_sie(znak=self.value_wynik.get(), typ='float')
        else:
            self._dzialanie_rowna_sie(znak=self.value_wynik.get(), typ='int')

    def _dzialanie_rowna_sie(self, znak=None, typ=None):
        """Wrapper dla słuchacza.
        Metoda wykonuje działanie rowna się.

        :param znak: rodzaj działania matematycznego *, /, +, -, %(modulo)
        :param typ: int lub float
        :return:
        """
        try:
            if znak == '+' and typ == 'int':
                self.value_wynik.set(str( int(self.value_y.get()) + int(self.value_x.get()) ))
            elif znak == '+' and typ == 'float':
                self.value_wynik.set(str( float(self.value_y.get()) + float(self.value_x.get()) ))
            elif znak == '-' and typ == 'int':
                self.value_wynik.set(str( int(self.value_y.get()) - int(self.value_x.get()) ))
            elif znak == '-' and typ == 'float':
                self.value_wynik.set(str( float(self.value_y.get()) - float(self.value_x.get()) ))
            elif znak == '*' and typ == 'int':
                self.value_wynik.set(str( int(self.value_y.get()) * int(self.value_x.get()) ))
            elif znak == '*' and typ == 'float':
                self.value_wynik.set(str( float(self.value_y.get()) * float(self.value_x.get()) ))
            elif znak == '/' and typ == 'int':
                self.value_wynik.set(str( int(self.value_y.get()) / int(self.value_x.get()) ))
            elif znak == '/' and typ == 'float':
                self.value_wynik.set(str( float(self.value_y.get()) / float(self.value_x.get()) ))
            elif znak == '%' and typ == 'int':
                self.value_wynik.set(str( int(self.value_y.get()) % int(self.value_x.get()) ))
            elif znak == '%' and typ == 'float':
                self.value_wynik.set(str( float(self.value_y.get()) % float(self.value_x.get()) ))
        except ValueError as e:
            messb.showerror("ValueError", "Zeruje zmienne :: {}".format(e))
            self.button_zeruj.invoke()

        self.value_x.set(self.value_wynik.get())
        self.value_y.set('0')

    def handler_zeruj(self, *args):
        """Słuchacz
        Metoda zerująca zmienne.
        :param *args:
        :return: None
        """
        self.value_wynik.set('0')
        self.value_y.set('0')
        self.value_x.set('0')

    def handler_dzialania(self, event):
        """Słuchacz
        Metoda dla przycisków działań.

        :param event:
        :return:
        """
        if self.children['plus'] == event.widget:
            self._typ_działania('+')
        elif self.children['minus'] == event.widget:
            self._typ_działania('-')
        elif self.children['mnozenie'] == event.widget:
            self._typ_działania('*')
        elif self.children['dzielenie'] == event.widget:
            self._typ_działania('/')
        elif self.children['modulo'] == event.widget:
            self._typ_działania('%')
        elif self.children['rownasie'] == event.widget:
            self._dzialaj()

    def handler_cyfry(self, event):
        """Słuchacz
        Metoda dla przycisków cyfr. Ustawia liczby na wejściu (tk.Enter)
        w zmiennej self.value_x

        :param event:
        :return:
        """
        tmp = self.value_x.get()
        #warunek dla liczb zmiennoprzecinkowych dodanie kropki (przecinka)
        if self.children['kropka'] == event.widget:
            #print('kropka')
            if not '.' in tmp:
                tmp += str('.')
                self.value_x.set(tmp)
        # dla cyfr
        else:
            for i in range(0,10):
                if self.children[CYFRY_DEC[i]] == event.widget:
                    if tmp == '0':
                        #print(tmp)
                        self.value_x.set(str(i))
                    else:
                        #print('else:tmp =', tmp)
                        tmp += str(i)
                        #print('else:tmp +=', tmp)
                        self.value_x.set(tmp)


if __name__ == '__main__':
    #
    # START PROGRAMU
    #
    kal = Kalkulator()
    kal.master.title("Prosty kalkulator")
    centrum_ekranu(kal.master)
    kal.mainloop()

#TODO dlugosc liczb
#TODO notacja naukowa ?
#TODO hex bin oct itp
#TODO opisy i dokumentacja